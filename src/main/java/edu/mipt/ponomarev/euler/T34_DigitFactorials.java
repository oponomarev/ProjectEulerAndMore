package edu.mipt.ponomarev.euler;

/**
 * Created by Oleg on 20.01.2016.
 */
public class T34_DigitFactorials {
    public static long factorial(long number){
        if (number == 1L || number == 0L){
            return 1;
        } else {
            return number * factorial(number-1);
        }
    }

    public static long sumOfTheFactorialsOfTheDigits(long number){
        String s = "" + number;
        char[] chars = s.toCharArray();
        long l = 0;
        for (char c : chars){
            l += factorial(c-48);
            if (l > number){
                break;
            }
        }
        return l;
    }
    public static void main(String[] args){
        long sum = 0;
        for (int i = 10; i < 10000000; i++){
            if (i == sumOfTheFactorialsOfTheDigits(i)){
                sum += i;
            }
        }
        System.out.println(sum);
    }
}
