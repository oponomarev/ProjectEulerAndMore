package edu.mipt.ponomarev.euler;

import java.util.List;

/**
 * Created by Oleg on 25.01.2016.
 */
public class T37_TruncatablePrimes {
    public static int limit = 1000000;
    public static List<Integer> primes = T35_CircularPrimes.getPrimeNumbers(limit);

    public static boolean isTruncatableFromRight(int number) {
        if (number < 10) {
            return false;
        }
        if (number > primes.get(primes.size() - 1)) {
            primes = T35_CircularPrimes.getPrimeNumbers(limit * 10);
        }
        while (number > 0) {
            if (!primes.contains(number)) {
                return false;
            } else {
                number = number / 10;
            }
        }
        return true;
    }

    public static boolean isTruncatableFromLeft(int number){
        if (number < 10) {
            return false;
        }
        if (number > primes.get(primes.size() - 1)) {
            primes = T35_CircularPrimes.getPrimeNumbers(limit * 10);
        }
        while (number > 0){
            if (!primes.contains(number)) {
                return false;
            } else {
                if (number < 10){
                    return true;
                }
                String s = "" + number;
                int position = 1;
                for (int i = 1; i < s.length(); i++){
                    position = position * 10;
                }
                number = number % position;
            }
        }
        return true;
    }
    public static void main(String[] args){
        System.out.println(isTruncatableFromRight(29));
        System.out.println(isTruncatableFromLeft(3797));
        int count = 0;
        long sum = 0;
        for (Integer i : primes){
            if (isTruncatableFromLeft(i) && isTruncatableFromRight(i)){
                count++;
                sum += i;
            }
            if (count == 11){
                break;
            }
        }
        System.out.println(count + " " + sum);
    }
}
