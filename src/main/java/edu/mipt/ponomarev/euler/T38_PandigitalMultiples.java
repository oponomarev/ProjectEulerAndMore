package edu.mipt.ponomarev.euler;

/**
 * Created by Oleg Ponomarev on 30.11.2015.
 */
public class T38_PandigitalMultiples
{
    public static void main (String[] args) {
        String digits = "123456789";
        int max = 0;
        for (int i = 9; i < 10000; i++) {
            String s = "";
            int integer = 1;
            while (s.length() < digits.length()) {
                s += i * integer;
                integer++;
            }
            if (T32_PandigitalProducts.haveSameDigits(digits, s) && max < Integer.parseInt(s)){
                max = Integer.parseInt(s);
            }
        }
        System.out.println (max);
    }
}
