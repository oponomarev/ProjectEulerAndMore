package edu.mipt.ponomarev.euler;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Scanner;

/**
 * Created by Oleg on 22.01.2016.
 */
public class T42_CodedTriangleNumbers {
    public static ArrayList<Long> triangleNumbers (long limit){
        ArrayList<Long> list = new ArrayList<Long>();
        for (int i = 1; i*(i+1) <= 2 * limit; i++){
            list.add((long)i*(i+1)/2);
        }
        return list;
    }
    public static ArrayList<Long> triangleNumbers (long from, long limit){
        ArrayList<Long> list = new ArrayList<Long>();
        for (int i = 1; i*(i+1) <= 2 * limit; i++){
            if ((long)i*(i+1)/2 >= from) {
                list.add((long)i*(i+1)/2);
            }
        }
        return list;
    }
    public static ArrayList<Long> numbers = triangleNumbers(1000);
    public static int wordValue(String s){
        s = s.toLowerCase();
        char[] chars = s.toCharArray();
        int sum = 0;
        for (char c : chars){
            sum += c - 96;
        }
        return sum;
    }

    public static void main(String[] args) throws FileNotFoundException{
        ArrayList<String> words = new ArrayList<String>();
        Scanner scanner = new Scanner(new File("D:\\GoogleDisk\\IdeaProjects\\Euler#2\\src\\main\\java\\edu\\mipt\\ponomarev\\euler\\p042_words.txt"));
        scanner.useDelimiter(",");
        while (scanner.hasNext()){
            String s = scanner.next();
            s = s.substring(1, s.length() - 1);
            words.add(s);
        }
        int count = 0;
        for (String s : words){
            if (numbers.contains(wordValue(s))){
                count++;
            }
        }
        System.out.println(count);
    }
}
