package edu.mipt.ponomarev.hh;

import java.util.ArrayList;
import java.util.List;

import static java.lang.Math.min;

/**
 * Created by Oleg Ponomarev on 28.09.2016.
 * Предположим, в один прекрасный день вы оказались на острове прямоугольный формы.
 * Ландшафт этого острова можно описать с помощью целочисленной матрицы размером MxN, каждый элемент которой
 * задаёт высоту соответствующей области острова над уровнем моря. В сезон дождей остров полностью заливает водой
 * и в низинах скапливается вода. Низиной будем считать такую область острова, клетки которой граничат с клетками,
 * большими по высоте. При этом диагональные соседи не учитываются, а уровень моря принимается за 0.
 * Ваша программа должна читать входные данные из stdin. В первой строке указывается количество островов K,
 * после чего в следующих строках описываются эти K островов. В первой строке описания острова задаются
 * его размеры N и M — целые числа в диапазоне [1, 50], разделённые пробелом.
 * В следующих строках описывается матрица NxM со значениями высот клеток острова,
 * которые могут принимать значения из диапазона [1, 1000].
 * Ваша программа должна выводить в stdout значения общего объёма воды,
 * скапливающейся на острове после сезона дождей для каждого из входных примеров.
 */
public class TropicalIsland {
    private int M = 3;
    private int N = 3;
    private int[] cells;
    private boolean[] discovered;
    private int accumulatedWater;

//    TODO заполнить массив, протестировать
    public TropicalIsland(int m, int n) {
        M = m;
        N = n;
        cells = new int[M * N];
        discovered = new boolean[cells.length];
        countAccumulatedWater();
    }

    private List<Integer> getHollows() {
        List<Integer> result = new ArrayList<>();
        for (int i = 0; i < cells.length; i++) {
            if (isHollow(cells[i])) {
                result.add(i);
            }
        }
        return result;
    }

    /**
     * @param index - номер ячейки
     * @return является ли ячейка c этим номером частью низины
     */
    private boolean isHollow(int index) {
        if (index <= 0 || index >= M * N) {
            throw new IllegalArgumentException("что-то не так с номерои ячейки");
        }
//        если ячейка граничит с морем, то надо вернуть false
        if (index < N
                || index > M * N - N - 1
                || N % index == 0
                || N % index == N - 1) {
            return false;
        }
//        граничит с побережьем и не ниже него - false
        if ((index >= N && index < 2 * N && cells[index] >= cells[index - N])
                || (index > M * N - 2 * N - 1 && index <= M * N - N - 1 && cells[index] >= cells[index + N])
                || N % index == 1 && cells[index] >= cells[index - 1]
                || N % index == N - 2 && cells[index] >= cells[index + 1]) {
            return false;
        }
        return cells[index] < cells[index - 1]
                || cells[index] < cells[index + 1]
                || cells[index] < cells[index + N]
                || cells[index] < cells[index - N];
    }

    private boolean hasHollows() {
        for (int i = 0; i < cells.length; i++) {
            if (isHollow(i)) {
                return true;
            }
        }
        return false;
    }

    private int depth(int index) {
        if (!isHollow(index)) {
            return 0;
        }
        return min(min(cells[index - 1] - cells[index], cells[index + 1] - cells[index]),
                min(cells[index - N] - cells[index], cells[index + N] - cells[index]));
    }

    private void countAccumulatedWater() {
        while (hasHollows()) {
            for (Integer index : getHollows()) {
                accumulatedWater += depth(index);
                cells[index] += depth(index);
            }
        }
    }

    public int getAccumulatedWater() {
        return accumulatedWater;
    }

    class Hollow {
        private List<Integer> cells;
        private int level;

    }
}