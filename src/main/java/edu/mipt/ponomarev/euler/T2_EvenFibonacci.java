package edu.mipt.ponomarev.euler;

/**
 * Created by Oleg Ponomarev on 13.08.2015.
 */
public class T2_EvenFibonacci {
    public static void main(String[] args) {
        System.out.println("Hello World!");
        int i1 = 1, i2 = 2, i3 = 0, number = 4000000, sum = 2;
        System.out.print(i1 + " " + i2 + " ");
        while (true) {
            i3 = i1 + i2;
            if (i3 > number) {
                System.out.println("; ");
                break;
            }
            if (i3 % 2 == 0) {
                sum = sum + i3;
            }
            i1 = i2;
            i2 = i3;
            System.out.print(i3 + " ");
        }
        System.out.println("Sum is " + sum);
    }
}
