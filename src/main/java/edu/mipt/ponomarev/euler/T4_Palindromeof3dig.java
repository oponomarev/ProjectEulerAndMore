package edu.mipt.ponomarev.euler;

/**
 * Created by Oleg Ponomarev on 13.08.2015.
 */
public class T4_Palindromeof3dig {
    public static void main(String[] args) {
        int num0,num1, num2 = 0, max = 0;

        for (int i = 1; i < 1000; i++) {
            for (int j = 1; j < 1000; j++) {
                num0 = i * j;
                num1 = num0;
                num2 = 0;
                while (num1 > 0) {
                    num2 = num2 * 10 + num1 % 10;
                    num1 = num1 / 10;
                }
                if (num0 == num2) {
                    System.out.println(num0);
                    if (num2 > max) {
                        max = num2;
                    }
                }
            }
        }
        System.out.println("max is " + max);
    }
}
