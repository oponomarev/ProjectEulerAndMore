import edu.mipt.ponomarev.euler.util.Utilities;
import org.junit.Assert;
import org.junit.Test;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

/**
 * @version 1.0
 * @Author Oleg Ponomarev
 * @since 20.11.2016
 */
public class UtilitiesTest {
    @Test
    public void sumOfLines() {
        StringBuilder builder1 = new StringBuilder( "12" );
        StringBuilder builder2 = new StringBuilder( "5" );
        Assert.assertEquals( "17", Utilities.sumOfLines( builder1, builder2 ).toString() );
    }

    @Test
    public void permutations() {
        assertTrue( Utilities.arePermutations( "41063625", "56623104"  ) );
        assertTrue( Utilities.arePermutations( "41063625", "66430125"  ) );
        assertFalse( Utilities.arePermutations( "212", "121"  ) );
    }
}
