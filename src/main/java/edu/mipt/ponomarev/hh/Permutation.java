package edu.mipt.ponomarev.hh;

/**
 * Created by Oleg on 06.09.2016.
 */
public class Permutation {
    public static boolean arePermutations(long a, long b) {
        String s1 = "" + a;
        String s2 = "" + b;
        if (s1.length() != s2.length()) {
            return false;
        }
        byte[] nums1 = new byte[10];
        byte[] nums2 = new byte[10];
        for (int i = 0; i < s1.length(); i++) {
            nums1[s1.charAt(i) - 48]++;
            nums2[s2.charAt(i) - 48]++;
        }
        for (int i = 0; i < nums2.length; i++) {
            if (nums1[i] != nums2[i]) {
                return false;
            }
        }
        return true;
    }

    public static void main(String[] args) {
        for (long i = 1; ; i++) {
            if (arePermutations(3 * i, 4 * i)) {
                System.out.println(i + "; " + 3 * i + " " + 4 * i);
                break;
            }
        }
        assert arePermutations(2, 2);
        assert arePermutations(125874, 521478);
    }
}
