package edu.mipt.ponomarev.euler;

import java.util.ArrayList;

/**
 * Created by Oleg on 24.01.2016.
 */
public class T49_PrimePermutations {
    static ArrayList<Integer> primes = T35_CircularPrimes.getPrimeNumbers(10000);
    public static boolean isPermutation(int num1, int num2){
        String s1 = "" + num1;
        String s2 = "" + num2;
        if (s1.length() != s2.length()){
            return false;
        }
        char[] numbers = new char[10];
        for (char i = '0'; i <= '9'; i++){
            numbers[i - 48] = i;
        }
        byte[] counts1 = new byte[10];
        byte[] counts2 = new byte[10];
        for (int i = 0; i < s1.length(); i++){
            char c = s1.charAt(i);
            for (int j = 0; j < numbers.length; j++){
                if (c == numbers[j]){
                    counts1[j]++;
                    break;
                }
            }
        }
        for (int i = 0; i < s2.length(); i++){
            char c = s2.charAt(i);
            for (int j = 0; j < numbers.length; j++){
                if (c == numbers[j]){
                    counts2[j]++;
                    break;
                }
            }
        }
        for (int i = 0; i < numbers.length; i++){
            if (counts1[i] != counts2[i]){
                return false;
            }
        }
        return true;
    }

    public static void main(String[] args){
        System.out.println(isPermutation(1487, 48171));
        int[] triplet = new int[3];
        for(Integer number : primes){
            if (number / 1000 == 0){
                continue;
            }
            int count = 1;
            triplet[0] = number;
            for (int i = primes.indexOf(number) + 1; i < primes.size(); i++){
                if (isPermutation(number, primes.get(i)) && primes.get(i) - triplet[count - 1] == 3330){
                    triplet[count++] = primes.get(i);
                }
                if (count == 3){
                    break;
                }
            }
            if (count >= 3){
                for (int i = 0; i < triplet.length; i++){
                    System.out.print(triplet[i] + " ");
                }
                System.out.println();
            }
        }
    }
}
