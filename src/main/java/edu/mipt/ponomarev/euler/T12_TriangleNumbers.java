package edu.mipt.ponomarev.euler;

/**
 * Created by Oleg Ponomarev on 13.08.2015.
 */
public class T12_TriangleNumbers {
    public static void main(String[] args){
        int i=0,k=1, j;
        long num=0L;
        while (i<=500){
            num=num+k;
            i=0;
            for (j=1;j<=(num / 2);j++){
                if ((num / j)<j){
                    i=i*2;
                    break;
                }
                if ((num % j)==0){
                    i++;
                }
            }
            k++;
        }
        System.out.print("Number is "+num+", number of divisors is "+i);
    }
}
