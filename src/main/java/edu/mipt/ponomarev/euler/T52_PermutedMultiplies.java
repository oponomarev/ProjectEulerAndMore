package edu.mipt.ponomarev.euler;

import static edu.mipt.ponomarev.euler.T49_PrimePermutations.isPermutation;

/**
 * Created by Oleg on 19.03.2016.
 */
public class T52_PermutedMultiplies {
    public static void main(String[] args){
        int i = 1;
        while (true){
            if (isPermutation (2*i, 3*i) &&
                    isPermutation(2*i,4*i) &&
                    isPermutation(2*i,5*i) &&
                    isPermutation(2*i,6*i)){
                System.out.println(i);
                break;
            }
            i++;
        }
    }
}
