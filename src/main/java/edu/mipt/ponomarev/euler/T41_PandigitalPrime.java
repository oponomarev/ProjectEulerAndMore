package edu.mipt.ponomarev.euler;

import org.omg.PortableInterceptor.SYSTEM_EXCEPTION;

import java.util.ArrayList;

/**
 * Created by Oleg on 22.01.2016.
 */
public class T41_PandigitalPrime {
    static ArrayList<Integer> primes;
    static {
        primes = T35_CircularPrimes.getPrimeNumbers(100000000);
    }
    public static boolean contains(char[] chars, char c){
        for (int i = 0; i < chars.length; i++){
            if (chars[i] == c){
                return true;
            }
        }
        return false;
    }
    public static boolean isPandigital(long number){
        String s = "" + number;
        char[] chars = s.toCharArray();
        for (char c = '1'; c < 49 + chars.length; c++){
            if (!contains(chars, c)){
                return false;
            }
        }
        return true;
    }
    public static void main(String[] args){
//        System.out.println(primes);
        for (int i = 0; i < primes.size(); i++){
            if (isPandigital(primes.get(i))){
                System.out.println(primes.get(i));
            }
        }
    }
}
