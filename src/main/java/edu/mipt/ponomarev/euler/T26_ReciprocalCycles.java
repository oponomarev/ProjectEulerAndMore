package edu.mipt.ponomarev.euler;

/**
 * Created by Oleg Ponomarev on 02.09.2015.
 *
 */
public class T26_ReciprocalCycles {
    public static String getRatio (int x, int y){
        if (x>=y){
            System.out.println ("wrong numbers");
            return "";
        }
        String s="0.";
        x=x*10;
        while (s.length()<1000){
            s=s+Integer.toString(x/y);
            x=(x%y)*10;
        }
        return s;
    }
    public static void main (String [] args){
        //System.out.println (Double.toString((double)1/731));
        String s,smax="";
        int kmax=0, imax=0;
        for (int i=2;i<1000;i++){
            s=getRatio(1,i);
            boolean p=true;
            int j=1, k=0;
            char[] mas=new char[1000];
            for (int q=0;q<1000;q++){
                mas[q]='a';
            }
            while (p){
                j++;
                if (j>=s.length()-1){
                    break;
                }
                for (int c=0;c<(k-2);c++){
                    if ((mas [c]==s.charAt(j))&&(mas [c+1]==s.charAt(j+1))&&(mas [c+2]==s.charAt(j+2)) ){
                        p=false;
                    }
                }
                if (p){
                    mas[k++]=s.charAt(j);
                }
            }
            if ((mas[0]=='0')&&(mas[1]=='0')){
                k--;
            }
            if (k>kmax){
                kmax=k;
                smax=s;
                imax=i;
            }
        }
        System.out.println(imax+" "+kmax);
    }
}
