package edu.mipt.ponomarev.euler;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

/**
 * Created by Oleg Ponomarev on 19.08.2015.
 */
public class T11_ProductOfGrid {
    public static final String PATH = "D://Box Sync//Java resources/T11.txt";

    public static void main(String args[]) throws FileNotFoundException {
        Scanner scanner = new Scanner(new File(PATH));
        int[][] mas = new int[20][20];
        int i = 0, j = 0;
        long grPr = 0L, var = 0L;
        while (scanner.hasNextInt()) {
            mas[i][j++] = scanner.nextInt();
            if (j > 19) {
                i++;
                j = 0;
            }
        }
        for (i = 0; i < 20; i++) {
            for (j = 0; j < 20; j++) {
                if ((i + 3) < 20) {
                    var = mas[i][j] * mas[i + 1][j] * mas[i + 2][j] * mas[i + 3][j];
                    if (var > grPr) {
                        grPr = var;
                    }
                }
                if (((i + 3) < 20) && ((j + 3) < 20)) {
                    var = mas[i][j] * mas[i + 1][j + 1] * mas[i + 2][j + 2] * mas[i + 3][j + 3];
                    if (var > grPr) {
                        grPr = var;
                    }
                }
                if ((j + 3) < 20) {
                    var = mas[i][j] * mas[i][j + 1] * mas[i][j + 2] * mas[i][j + 3];
                    if (var > grPr) {
                        grPr = var;
                    }
                }
                if (((i - 3) >= 0) && ((j + 3) < 20)) {
                    var = mas[i][j] * mas[i - 1][j + 1] * mas[i - 2][j + 2] * mas[i - 3][j + 3];
                    if (var > grPr) {
                        grPr = var;
                    }
                }
            }
        }
        System.out.print("Max product is "+grPr);
    }
}
