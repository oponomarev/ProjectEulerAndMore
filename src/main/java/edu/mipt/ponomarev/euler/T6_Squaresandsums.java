package edu.mipt.ponomarev.euler;

/**
 * Created by Oleg Ponomarev on 13.08.2015.
 */
public class T6_Squaresandsums {
    public static void main(String[] args){
        int sum1=0,sum2=0,i;
        for (i=1;i<11;i++){
            sum1=sum1+i;
            sum2=sum2+i*i;
        }
        sum1=sum1*sum1;
        System.out.println("delta is "+(sum1-sum2));
    }
}
