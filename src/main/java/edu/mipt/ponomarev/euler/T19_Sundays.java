package edu.mipt.ponomarev.euler;

/**
 * Created by Oleg Ponomarev on 20.08.2015.
 */
public class T19_Sundays {
    public static int getDays(int x, int y){
        switch (x){
            case 1:
            case 3:
            case 5:
            case 7:
            case 8:
            case 10:
            case 12:
                return 31;
            case 4:
            case 6:
            case 9:
            case 11:
                return 30;
            case 2:
                return y+28;
        }
        return 0;
    }
    public static void main (String[] args){
        int year, month=1,day=1,sum=0, x, leap;
        for (year=1900;year<2001;year++){
            if ((year%4==0) && (year!=1900)){
                leap=1;
            }
            else {
                leap=0;
            }
            for (month=1;month<13;month++){
                if (((day%7)==0) && (year>1900)){
                    sum++;
                }
                day=(day+getDays(month,leap))%7;
            }
        }
        System.out.println(sum);
    }
}
