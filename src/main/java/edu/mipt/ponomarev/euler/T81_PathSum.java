package edu.mipt.ponomarev.euler;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

/**
 * Created by Oleg Ponomarev on 16.09.2016.
 */
public class T81_PathSum {
    private static int[][] input;
    // костыль!
    private static final int SIZE = 80;
    static {
        input = new int[SIZE][];
        List<int[]> list = new ArrayList<>();
        Scanner scanner = null;
        try {
            scanner = new Scanner(new File("D:\\GoogleDisk\\IdeaProjects\\Euler#2\\src\\main\\resources\\p081_matrix.txt"));
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        while (scanner.hasNextLine()) {
            String s = scanner.nextLine();
            String[] array = s.split(",");
            int[] nums = new int[array.length];
            for (int i = 0; i < array.length; i++) {
                nums[i] = Integer.parseInt(array[i]);
            }
            list.add(nums);
        }
        list.toArray(input);

    }
    private static boolean isLegalIndexes (int i, int j) {
        return i >= 0 && i < input.length && j >= 0 && j < input[i].length;
    }

    private static int min (int a, int b) {
        return a < b ? a : b;
    }

    public static void main(String[] args) {
        for (int i = 0; i < input.length; i++) {
            for (int j = 0; j < input[i].length; j++) {
                if (isLegalIndexes(i - 1, j) && isLegalIndexes(i, j - 1)) {
                    input[i][j] += min(input[i - 1][j], input[i][j - 1]);
                } else if (isLegalIndexes(i - 1, j)) {
                    input[i][j] += input[i - 1][j];
                } else if (isLegalIndexes(i, j - 1)) {
                    input[i][j] += input[i][j - 1];
                }
            }
        }
        System.out.println(input[input.length - 1][input.length - 1]);
    }
}
