package edu.mipt.ponomarev.euler;

import java.util.ArrayList;

/**
 * Created by Oleg Ponomarev on 30.11.2015.
 */
public class T32_PandigitalProducts
{
    public static void main (String[] args) {
        ArrayList<Integer> products = new ArrayList<Integer>();
        ArrayList<Integer> mult1 = new ArrayList<Integer>();
        ArrayList<Integer> mult2 = new ArrayList<Integer>();
        String digits = "123456789";
        //String s = ""+47+173+8131;
        //System.out.println(haveSameDigits(digits, s));
        for (int i = 1; i < 100; i++) {
            for (int j = 100; j < 10000; j++) {
                int product = i * j;
                String s = "" + i + j + product;
                if (haveSameDigits(digits, s)){
                    boolean p = false;
                    for (Integer number : products){
                        if (number == product){
                            p = true;
                            break;
                        }
                    }
                    if (!p){
                        products.add(product);
                        mult1.add(i);
                        mult2.add(j);
                    }
                }
            }
        }
        long sum = 0L;
        for (int i = 0; i < products.size(); i++){
            System.out.println(products.get(i) + " " + mult1.get(i) + " " + mult2.get(i));
            sum += products.get(i);
        }
        System.out.println(sum);
    }
    public static boolean haveSameDigits (String s1, String s2){
        if (s1.length() != s2.length()){
            return false;
        } else {
            int i;
            byte count = 0;
            for (i = 0; i < s1.length(); i++) {
                count = 0;
                for (int j = 0; j < s2.length(); j++){
                    if (s1.charAt(i) == s2.charAt(j)){
                        count++;
                    }
                }
                if (count != 1){
                    break;
                }
            }
            if (count == 1 && i == s1.length()){
                return true;
            }
            return false;
        }
    }
}
