package edu.mipt.ponomarev.euler;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.*;

/**
 * Created by Oleg on 06.01.2016.
 */
public class T18_MaxPathSum1 {
    public static void main(String[] args) {
        System.out.println(getSumInACleverWay(testInput));
        System.out.println(getSumInACleverWay(input));
        /*for (int i = 0; i < testInput.length; i++) {
            for (int j = 0; j < testInput[i].length; j++) {
                System.out.print(testInput[i][j] + " ");
            }
            System.out.println();
        }*/
    }

    private static final String PATH = "D:\\GoogleDisk\\IdeaProjects\\Euler#2\\src\\main\\resources\\T18.txt";
    private static final String PATH_TEST = "D:\\GoogleDisk\\IdeaProjects\\Euler#2\\src\\main\\resources\\T18-test.txt";

    public static long[][] fillInput(String path) {
        List<long[]> lines = new ArrayList<>();
        try (Scanner scanner = new Scanner(Paths.get(path))) {
            String s;
            while (scanner.hasNextLine()) {
                s = scanner.nextLine();
                String[] almostNumbers = s.split(" ");
                long[] numbers = new long[almostNumbers.length];
                for (int i = 0; i < numbers.length; i++) {
                    numbers[i] = Long.parseLong(almostNumbers[i]);
                }
                lines.add(numbers);
            }
        } catch (IOException e) {
            System.out.println("input is incorrect");
        }
        return lines.toArray(new long[lines.size()][]);
    }

    public static long[][] input = fillInput(PATH);
    public static long[][] testInput = fillInput(PATH_TEST);

    public static long getSumInACleverWay(long[][] input) {
        for (int depth = input.length - 2; depth >= 0; depth--) {
            for (int index = 0; index < input[depth].length; index++) {
                long candidate1 = input[depth + 1][index];
                long candidate2 = input[depth + 1][index + 1];
                input[depth][index] += candidate1 > candidate2 ? candidate1 : candidate2;
            }
        }
        return input[0][0];
    }
}

