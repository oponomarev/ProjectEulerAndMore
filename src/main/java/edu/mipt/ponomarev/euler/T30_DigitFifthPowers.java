package edu.mipt.ponomarev.euler;

/**
 * Created by Oleg on 20.01.2016.
 */
public class T30_DigitFifthPowers {
    public static int toFifthPower(int number) {
        int result = 1;
        for (int i = 0; i < 5; i++) {
            result = result * number;
        }
        return result;
    }

    public static int sumOfFifthPowersOfDigits(int number){
        String s = "" + number;
        char[] chars = s.toCharArray();
        int result = 0;
        for (char c : chars){
            result += toFifthPower(c-48);
        }
        return result;
    }
    public static void main(String[] args) {
        int sum = 0;
        for (int i = 10; i < 1000000; i++){
            if (i == sumOfFifthPowersOfDigits(i)){
                sum += i;
            }
        }
        System.out.println(sum);
    }
}
