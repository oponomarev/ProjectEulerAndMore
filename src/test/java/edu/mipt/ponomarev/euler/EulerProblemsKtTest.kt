package edu.mipt.ponomarev.euler

import org.junit.Test
import kotlin.test.assertEquals

internal class EulerProblemsKtTest {

    @Test
    fun sumOfMultiples3and5Test() {
        assertEquals(233168, sumOfMultiples3and5())
    }

    @Test
    fun fibonacciSumTest() {
        assertEquals(4613732, fibonacciSum())
    }

    @Test
    fun largestPrimeFactorTest() {
        assertEquals(6857, largestPrimeFactor(600851475143))
    }

    @Test
    fun largestPalindromicTest() {
        assertEquals(906609, largestPalindromic())
    }

    @Test
    fun smallestMultipleTest() {
        assertEquals(232792560, smallestMultiple(20))
    }

    @Test
    fun differenceBetweenSquareOfSumAndSumOfSquaresTest() {
        assertEquals(25164150, differenceBetweenSquareOfSumAndSumOfSquares(100))
    }

    @Test
    fun ithPrimeNumberTest() {
        assertEquals(104743, ithPrimeNumber(10001))
    }

    @Test
    fun abcTest() {
        assertEquals(31875000, abc())
    }

    @Test
    fun sumOfPrimesBelow2mTest() {
        assertEquals(142913828922, sumOfPrimesBelow(2_000_000))
    }

    @Test(timeout = 30_000)
    fun sumOfPrimesBelow10Test() {
        assertEquals(17, sumOfPrimesBelow(10))
    }
}