package edu.mipt.ponomarev.euler;

/**
 * Created by Oleg Ponomarev on 01.12.2015.
 */
public class T31_CoinSums
{
    public static final int sum = 200;
    public static final int[] coins = {1, 2, 5, 10, 20, 50, 100, 200};
    public static boolean isSum (int[] coefficients) throws Exception{
        if (coefficients.length != coins.length){
            throw new IllegalArgumentException();
        }
        int sum = 0;
        for (int i = 0; i < coins.length; i++){
            sum += coins[i] * coefficients[i];
        }
        return sum == T31_CoinSums.sum;
    }
    public static int count = 0;
    public static void main (String[] args){
        for (int i = 0; i < 201; i++){
            for (int j = 0; j < 101; j++){
                for (int k = 0; k < 41; k++){
                    for (int l = 0; l < 21; l++){
                        for (int m = 0; m < 11; m++){
                            for (int n = 0; n < 5; n++){
                                for (int p = 0; p < 3; p++){
                                    for (int q = 0; q < 2; q++){
                                        int[] coefficients = {i, j, k, l, m, n, p, q};
                                        try {
                                            if(isSum(coefficients)){
                                                count++;
                                            }
                                        }
                                        catch (Exception e){
                                            e.printStackTrace();
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        System.out.println(count);
    }
}
