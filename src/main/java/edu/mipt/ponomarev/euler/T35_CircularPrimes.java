package edu.mipt.ponomarev.euler;

import org.omg.PortableInterceptor.SYSTEM_EXCEPTION;

import java.util.ArrayList;

/**
 * Created by Oleg Ponomarev on 30.11.2015.
 */
public class T35_CircularPrimes
{
    static ArrayList<Integer> getPrimeNumbers(int limit){
        ArrayList<Integer> list = new ArrayList<Integer>();
        list.add(2);
        for (int i = 3; i <= limit; i++){
            boolean p = false;
            /*int k = 0;
            while (k * k <= i){
                k++;
            }*/
            for (int j = 0; list.get(j)*list.get(j) <= i; j++){
                if (i % list.get(j) == 0){
                    p = true;
                    break;
                }
            }
            if (!p){
                list.add(i);
            }
        }
        return list;
    }
    static ArrayList<Integer> getRotations(int number){
        ArrayList<Integer> list = new ArrayList<Integer>();
        char[] digits = Integer.toString(number).toCharArray();
        for (int i = 0; i < digits.length; i++){
            String s="";
            for (int j = i; j < i + digits.length; j++){
                s += digits[j % digits.length];
            }
            list.add(Integer.parseInt(s));
        }
        return list;
    }
    public static void main (String[] args){
        int sum = 0;
        ArrayList<Integer> list = getPrimeNumbers(1000000);
        //System.out.print(rotations);
        for (Integer primeNumber : list){
            ArrayList<Integer> rotations = getRotations(primeNumber);
            int count = 0;
            for (Integer rotation : rotations){
                if (list.contains(rotation)){
                    count++;
                }
            }
            if (count == rotations.size()){
                sum++;
            }
        }
        System.out.println(list);
    }
}
