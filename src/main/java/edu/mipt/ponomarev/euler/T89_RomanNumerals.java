package edu.mipt.ponomarev.euler;

import java.io.BufferedReader;
import java.io.FileReader;
import java.util.ArrayList;

/**
 * Created by Oleg Ponomarev on 21.12.2015.
 */
public class T89_RomanNumerals
{
    public static int romanToArabic (String s) {
        int sum = 0;
        char[] chars = s.toCharArray();
        int[] ints = new int[chars.length];
        for (int i = 0; i < ints.length; i++) {
            switch (chars[i]) {
                case 'I':
                    ints[i] = 1;
                    break;
                case 'V':
                    ints[i] = 5;
                    break;
                case 'X':
                    ints[i] = 10;
                    break;
                case 'L':
                    ints[i] = 50;
                    break;
                case 'C':
                    ints[i] = 100;
                    break;
                case 'D':
                    ints[i] = 500;
                    break;
                case 'M':
                    ints[i] = 1000;
                    break;
                default:
                    throw new IllegalArgumentException("Error reading number");
            }
        }
        int i;
        for (i = 0; i < ints.length - 1; i++) {
            if (ints[i] >= ints[i + 1]) {
                sum += ints[i];
            } else {
                sum += ints[i + 1] - ints[i];
                i++;
            }
        }
        if (i == ints.length - 1) {
            sum += ints[i];
        }
        return sum;
    }

    public static boolean isMinimalRoman (String s) {
        s = s.toUpperCase();
        if (s.contains("IIII")) {
            return false;
        }
        if (s.contains("VV")) {
            return false;
        }
        if (s.contains("XXXX")) {
            return false;
        }
        if (s.contains("LL")) {
            return false;
        }
        if (s.contains("CCCC")) {
            return false;
        }
        if (s.contains("DD")) {
            return false;
        }
        return true;
    }

    public static String arabicToRoman (int number) {
        String s = "";
        int M = number / 1000;
        for (int i = 0; i < M; i++) {
            s += 'M';
        }
        number = number % 1000;
        if (number / 900 == 1) {
            s += "CM";
            number = number % 900;
        } else if (number / 500 == 1) {
            s += "D";
            number = number % 500;
        } else if (number / 400 == 1) {
            s += "CD";
            number = number % 400;
        }
        if (number / 100 > 0){
            for (int i = 0; i < number / 100; i++){
                s += "C";
            }
            number = number % 100;
        }
        if (number / 90 == 1){
            s += "XC";
            number = number % 90;
        } else if (number / 50 == 1){
            s += "L";
            number = number % 50;
        } else if (number / 40 == 1){
            s += "XL";
            number = number % 40;
        }
        if (number / 10 > 0){
            for (int i = 0; i < number / 10; i++){
                s += "X";
            }
            number = number % 10;
        }
        if (number / 9 == 1){
            s += "IX";
            number = number % 9;
        } else if (number / 5 == 1){
            s += "V";
            number = number % 5;
        } else if (number / 4 == 1) {
            s += "IV";
            number = number % 4;
        }
        if (number == 3){
            s += "III";
            number = number % 3;
        } else if (number == 2){
            s += "II";
            number = number % 2;
        } else if (number == 1){
            s += "I";
            number = 0;
        }
        if (number == 0) {
            return s;
        } else {
            return "error";
        }
    }

    public static void main (String[] args) {
        BufferedReader reader;
        int sumSource = 0;
        int sumMinimal = 0;
        ArrayList<String> romanNumbers = new ArrayList<String>();
        try {
            reader = new BufferedReader(new FileReader("D:\\GoogleDisk\\IdeaProjects\\Euler#2\\src\\main\\java\\edu\\mipt\\ponomarev\\euler\\p089_roman.txt"));
            String s;
            while ((s = reader.readLine()) != null) {
                romanNumbers.add(s);
            }
            reader.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        for (int i = 0; i < romanNumbers.size(); i++){
            sumSource += romanNumbers.get(i).length();
            sumMinimal += arabicToRoman(romanToArabic(romanNumbers.get(i))).length();
        }
        System.out.println(sumSource + " " + sumMinimal + " " + "delta is " + (sumSource - sumMinimal));
    }
}
