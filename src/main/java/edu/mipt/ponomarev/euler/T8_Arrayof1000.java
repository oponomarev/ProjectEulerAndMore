package edu.mipt.ponomarev.euler;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

/**
 * Created by Oleg Ponomarev on 13.08.2015.
 */
public class T8_Arrayof1000 {
    public static final String PATH = "D://Box Sync//Java resources/T8.txt";

    public static void main(String[] args) throws FileNotFoundException {
        Scanner scanner = new Scanner(new File(PATH));
        char[] masofChar = new char[1000];
        int[] masofInt = new int[1000];
        int i = 0, j;
        long grPow = 0L, grPow1;
        String s1 = scanner.nextLine();
        j = s1.length();
        for (i = 0; i < j; i++) {
            masofChar[i] = s1.charAt(i);
            masofInt[i] = Character.getNumericValue(masofChar[i]);
        }
        for (i = 0; i < (j - 13); i++) {
            grPow1 = masofInt[i];
            for (int k = 1; k < 13; k++) {
                grPow1 = grPow1 * masofInt[i + k];
            }
            if (grPow1 > grPow) {
                grPow = grPow1;
            }
        }
        System.out.println("hola " + grPow);
        //i=0;
        //while (masofInt[i]!=0){
        //    System.out.print(masofInt[i++]+" ");

    }
}