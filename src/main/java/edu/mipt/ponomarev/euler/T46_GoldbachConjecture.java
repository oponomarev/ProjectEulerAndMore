package edu.mipt.ponomarev.euler;

import java.util.ArrayList;

/**
 * Created by Oleg Ponomarev on 30.11.2015.
 */
public class T46_GoldbachConjecture
{
    static ArrayList<Integer> getSquares(int limit){
        ArrayList<Integer> list = new ArrayList<Integer>();
        for (int i = 1; i * i <= limit; i++){
            list.add(i*i);
        }
        return list;
    }
    public static void main (String[] args){
        ArrayList<Integer> primes = T35_CircularPrimes.getPrimeNumbers(10000);
        ArrayList<Integer> squares = getSquares(10000);
        int number = 9;
        while (true){
            if (!primes.contains(number)){
                boolean p = false;
                for (int i = 0; squares.get(i) * 2 < number; i++){
                    if (primes.contains(number - 2 * squares.get(i))){
                        p = true;
                        break;
                    }
                }
                if (!p){
                    System.out.println(number);
                    break;
                }
            }
            number +=2;
        }
        //System.out.println(squares);
    }
}
