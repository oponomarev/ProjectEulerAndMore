package edu.mipt.ponomarev.euler;

import java.util.ArrayList;

/**
 * Created by Oleg Ponomarev on 17.12.2015.
 */
public class T92_SquareDigitChains
{
    public static int[] toDigitArray (int number){
        ArrayList<Integer> list = new ArrayList<Integer>();
        while (number > 0){
            list.add(number % 10);
            number = number / 10;
        }
        int[] array = new int[list.size()];
        for (int i = 0; i < list.size(); i++){
            array[i] = list.get(list.size() - 1 - i);
        }
        return array;
    }

    public static int sumOfSquaresOfTheDigits (int number){
        int[] array = toDigitArray(number);
        int sum = 0;
        for (int i = 0; i < array.length; i++){
            sum += array[i]*array[i];
        }
        return sum;
    }

    public static void main (String[] args){
        int count = 0;
        for (int i = 2; i < 10000000; i++){
            int number = i;
            while (true){
                number = sumOfSquaresOfTheDigits(number);
                if (number == 1){
                    break;
                } else if (number == 89){
                    count++;
                    break;
                }
            }
        }
        System.out.println(count);
    }
}
