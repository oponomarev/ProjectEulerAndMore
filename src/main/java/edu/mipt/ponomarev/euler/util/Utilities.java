package edu.mipt.ponomarev.euler.util;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Oleg Ponomarev on 30.07.2016.
 */
public class Utilities {
    private static String regex = "\\d+";

    public static StringBuilder sumOfLines (StringBuilder... lines) {
        StringBuilder result = new StringBuilder();
        int remainder = 0;
        int maxLength = 0;
        for (StringBuilder line : lines) {
            if (line.length() > maxLength) {
                maxLength = line.length();
            }
        }
        for (StringBuilder line : lines) {
            if (line.length() < maxLength) {
                while (maxLength != line.length()) {
                    line.insert(0, '0');
                }
            }
        }
        for (int i = lines[0].length() - 1; i >= 0; i--) {
            int sum = remainder;
            for (StringBuilder line : lines) {
                sum += Character.getNumericValue(line.charAt(i));
            }
            if (sum > 9) {
                remainder = sum / 10;
                sum = sum % 10;
            } else {
                remainder = 0;
            }
            result.insert(0, sum);
        }
        if (remainder > 0) {
            result.insert(0, remainder);
        }
        return result;
    }

    public static String sumOfLines (String... lines) {
        StringBuilder[] stringBuilders = new StringBuilder[lines.length];
        for (int i = 0; i < lines.length; i++) {
            stringBuilders[i] = new StringBuilder(lines[i]);
        }
        return sumOfLines(stringBuilders).toString();
    }

    private static StringBuilder multipleLineByNumber (String a, int currentNumberFromB) {
        if (currentNumberFromB < 0 || currentNumberFromB > 9) {
            throw new IllegalArgumentException("number is wrong");
        }
        int remainder = 0;
        StringBuilder line = new StringBuilder();
        for (int j = a.length() - 1; j >= 0; j--) {
            int currentNumberFromA = Character.getNumericValue(a.charAt(j));
            int product = currentNumberFromA * currentNumberFromB + remainder;
            if (product < 10) {
                remainder = 0;
            } else {
                remainder = product / 10;
                product = product % 10;
            }
            line.insert(0, product);
        }
        if (remainder > 0) {
            line.insert(0, remainder);
        }
        return line;
    }

    public static String getProduct (String a, String b) {
        if (!a.matches(regex) || !b.matches(regex)) {
            throw new IllegalArgumentException("an argument contains not only digits");
        }
        if (a.equals("1")) {
            return b;
        }
        if (b.equals("1")) {
            return a;
        }
        StringBuilder[] lines = new StringBuilder[b.length()];
//        fill in each line
        for (int i = 0; i < lines.length; i++) {
            int currentNumberFromB = Character.getNumericValue(b.charAt(b.length() - 1 - i));
            lines[i] = multipleLineByNumber(a, currentNumberFromB);
            for (int k = 0; k < i; k++) {
                lines[i].append(0);
            }
        }
//        trim length of all lines to the last one
        int maxSize = lines[lines.length - 1].length();
        for (StringBuilder line : lines) {
            while (line.length() < maxSize) {
                line.insert(0, 0);
            }
        }
//        sum lines
        return sumOfLines(lines).toString();
    }

    public static String getBthPowerOfA (String a, String b) {
        if (a.equals("0") && b.equals("0")) {
            throw new IllegalArgumentException("It's undefined for 0^0");
        } else if (a.equals("0")) {
            return "1";
        } else if (b.equals("0") || b.equals("1")) {
            return a;
        } else {
            return getProduct(getBthPowerOfA(a, "" + (Integer.parseInt(b) - 1)), a);
        }
    }

    /**
     * @return - list of primes below limit (not including)
     */
    public static List <Integer> getPrimes (int limit) {
        List <Integer> result = new ArrayList <>();
        OUTER:
        for (int i = 2; i < limit; i++) {
            for (int j : result) {
                if (i % j == 0) {
                    continue OUTER;
                }
            }
            result.add(i);
        }
        return result;
    }

    public static boolean arePermutations (String number1, String number2) {
        if (number1 == null || number2 == null) {
            return false;
        }
        if (number1.length() != number2.length()) {
            return false;
        }
        if (number1.charAt(0) == '-' || number2.charAt(0) == '-') {
            throw new IllegalArgumentException("Number range overflow: " + number1 + ", " + number2);
        }

        byte[] numbers1 = new byte[10];
        byte[] numbers2 = new byte[10];
        for (int index = 0; index < number1.length(); index++) {
            numbers1[number1.charAt(index) - '0']++;
        }
        for (int index = 0; index < number2.length(); index++) {
            numbers2[number2.charAt(index) - '0']++;
        }

        for (int index = 0; index < numbers1.length; index++) {
            if (numbers1[index] != numbers2[index]) {
                return false;
            }
        }

        return true;
    }

}
