package edu.mipt.ponomarev.euler;

/**
 * Created by Oleg Ponomarev on 13.08.2015.
 */
public class T7_10001Prime {
    public static void main(String[] args){
        int i=1,j,k;
        boolean p;
        long num=2L;
        while (i<10001){
            num++;
            k=1;
            while (k*k<=num){
                k++;
            }
            p=true;
            for (j=2;j<k+1;j++){
                if (num % j ==0){
                    p=false;
                }
            }
            if (p){
                i++;
            }
        }
        System.out.print("the number is " + num + " and its numerical order is " + i);
    }
}
