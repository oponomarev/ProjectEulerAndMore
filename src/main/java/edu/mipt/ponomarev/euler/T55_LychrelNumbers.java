package edu.mipt.ponomarev.euler;

import static edu.mipt.ponomarev.euler.T36_DoubleBasePalindroms.isPalindromic;
import static edu.mipt.ponomarev.euler.util.Utilities.sumOfLines;

/**
 * Created by Oleg Ponomarev on 25.09.2016  .
 */
public class T55_LychrelNumbers {
    private static final int LIMIT = 10_000;
    private static int count;

    public static void main(String[] args) {
        for (int i = 1; i < LIMIT; i++) {
            boolean isLychrel = true;
            StringBuilder number = new StringBuilder(Integer.toString(i));
            for (int j = 1; j <= 50; j++) {
                StringBuilder reverseNumber = new StringBuilder(number);
                reverseNumber.reverse();
                number = sumOfLines(number, reverseNumber);
                if (isPalindromic(number.toString())) {
                    isLychrel = false;
                    break;
                }
            }
            if (isLychrel) {
                count++;
            }
        }
        System.out.println(count);
    }
}
