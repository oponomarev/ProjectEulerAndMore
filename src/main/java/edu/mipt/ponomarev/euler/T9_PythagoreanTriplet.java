package edu.mipt.ponomarev.euler;

/**
 * Created by Oleg Ponomarev on 13.08.2015.
 */
public class T9_PythagoreanTriplet {
    public static void main (String[] args){
        int i,j,k;
        for (i=1;i<1000;i++){
            for (j=i+1;j<1000;j++){
                for (k=j+1;k<1000;k++){
                    if ((i+j+k)==1000 && (i*i+j*j)==k*k){
                        System.out.print (i+" "+j+" "+k+" "+(i*j*k));
                        break;
                    }
                }
            }
        }
    }
}
