package edu.mipt.ponomarev.euler;

/**
 * Created by Oleg Ponomarev on 20.08.2015.
 */
public class T16_2inpowerof1000 {
    public static void main(String[] args){
        int i,len,sum=0,res=0,quant;
        int[] num=new int[1000];
        char c;
        String s="2";
        for (int j=1;j<1000;j++){
            len=s.length();
            quant=0;
            while (len>0){
                c=s.charAt(len-1);
                num[len-1]=Character.getNumericValue(c)*2+res;
                if ((num[len-1]>9) && (len-1)>0){
                    res=num[len-1]/10;
                    num[len-1]=num[len-1]%10;
                }
                else {
                    res=0;
                }
                len--;
                quant++;
            }
            s="";
            for (i=0;i<quant;i++){
                s=s+num[i];
            }
        }
        for (i=0;i<s.length();i++){
            sum=sum+Character.getNumericValue(s.charAt(i));
        }
        System.out.println(sum);
    }
}
