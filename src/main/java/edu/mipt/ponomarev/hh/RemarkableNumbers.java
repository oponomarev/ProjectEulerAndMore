package edu.mipt.ponomarev.hh;

/**
 * Created by Oleg on 06.09.2016.
 */
public class RemarkableNumbers {
    private static final int LIMIT = 7200000;

    public static boolean isRemarkable(int number) {
        String s = "" + number;
        boolean[] check = new boolean[s.length()];
        for (int j = 0; j < s.length() - 1; j++) {
            byte sum = 0;
            for (int i = j; i < s.length(); i++) {
                sum += s.charAt(i) - 48;
                if (sum == 10) {
                    for (int k = j; k < check.length; k++) {
                        if (k <= i) {
                            check[k] = true;
                        } else if (s.charAt(k) == '0') {
                            check[k] = true;
                        } else break;
                    }
                    break;

                } else if (sum > 10) {
                    break;
                }
            }
        }
        for (boolean flag : check) {
            if (!flag) {
                return false;
            }
        }
        return true;
    }

    public static void main(String[] args) {
        int count = 0;
        for (int i = 0; i < LIMIT; i++) {
            if (isRemarkable(i)) {
                count++;
            }
        }
        System.out.println(count);

        assert isRemarkable(3523014);
        assert !isRemarkable(28546);
        assert isRemarkable(190);
        assert isRemarkable(802000);
    }
}
