package edu.mipt.ponomarev.hh;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Oleg Ponomarev on 20.09.2016.
 * Наименьшее число m, такое, что m! делится без остатка на 10 — это m=5 (5! = 120).
 * Аналогично, наименьшее число m, такое, что m! делится без остатка на 25 — это m=10.
 * В общем случае, значение функции s(n) равно наименьшему числу m, такому что m! без остатка делится на n.
 * Определим функцию S(M, N) = ∑s(n) для всех n ∈ [M, N]. К примеру, S(6, 10) = 3 + 7 + 4 + 6 + 5 = 25.
 * Найдите S(540000000, 550000000).
 */
public class Factorial {
    private static final int LEFT_BOUND = 540_000_000;
    private static final int RIGHT_BOUND = 550_000_000;
    private static int[] results = new int[RIGHT_BOUND - LEFT_BOUND + 1];
    private static List<Integer> primes = new ArrayList<>(); //Utilities.getPrimes(RIGHT_BOUND);
//    static {
//        fillResults();
//    }
    private static void fillResults () {
        int m = 1;
        double factorial = 1;
        while (factorial < LEFT_BOUND) {
            m++;
            factorial *= m;
        }
        for (int i = LEFT_BOUND; i <= RIGHT_BOUND; i++) {
            if (results[i - LEFT_BOUND] != 0) {
                continue;
            }
            if (primes.contains(i)) {
                results[i - LEFT_BOUND] = i;
            }
            for (int j = i; j <= factorial && j <= RIGHT_BOUND; j++) {
                if (factorial % j == 0) {
                    results[j - LEFT_BOUND] = m;
                }
            }
            m++;
            factorial *= m;
        }
    }
   private static double S(int n) {
        if (results[n - LEFT_BOUND] != 0) {
            return results[n];
        }
        int m = 1;
        double factorial = 1;
        while (true) {
            double res = factorial % n;
            if (factorial % n == 0.0) {
                return  m;
            } else {
                m++;
                factorial *= m;
            }
        }
    }

    private static double S(int m, int n) {
        double sum = 0;
        for (int i = m; i <= n; i++) {
            sum += results[i];
        }
        return sum;
    }

    public static void main(String[] args) {
//        assert results[10] == 5;
//        assert results[25] == 10;
//        assert S(6, 10) == 25;
//
//        System.out.println(results[550000000]);
//        System.out.println(results[549999999]);
//        System.out.println(S(540_000_000, 550_000_000));
        System.out.println(S(540_000_000));
    }
}
