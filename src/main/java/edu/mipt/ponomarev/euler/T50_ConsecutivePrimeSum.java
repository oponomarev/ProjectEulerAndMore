package edu.mipt.ponomarev.euler;

import java.util.List;

/**
 * Created by Oleg on 20.01.2016.
 */
public class T50_ConsecutivePrimeSum {
    public static void main(String[] args){
        List<Integer> list = T35_CircularPrimes.getPrimeNumbers(1000000);
        int sum = 0;
        int quantity = 0;
        for (int i = 1; i < list.size(); i++){
            for (int j = 0; j < i; j++){
                int count = 0;
                int currentSum = 0;
                int k = j;
                while (currentSum < list.get(i)){
                    currentSum += list.get(k++);
                    count++;
                    if (currentSum == list.get(i) && count > quantity){
                        quantity = count;
                        sum = currentSum;
                    }
                }
            }
        }
        System.out.println(sum);
    }
}
