package edu.mipt.ponomarev.euler;

/**
 * Created by Oleg Ponomarev on 13.08.2015.
 */
public class T5_SmallestMultiple {
    public static void main (String[] args){
        int num=1,i=1,n=20;
        boolean p=true;
        while (i<(n+1)){
                if (num % i > 0) {
                    i=1;
                    num++;
                }
                else {
                    i++;
                }
            }
        System.out.println(num);
    }
}

