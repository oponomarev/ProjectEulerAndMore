package edu.mipt.ponomarev.euler;

/**
 * Created by Oleg Ponomarev on 26.08.2015.
 */
public class T25_1000DigitFib {
    public static void main(String[] args) {
        String[] s = new String[3];
        s[0] = "1";
        s[1] = "1";
        s[2] = "";
        int i = 2;
        while (s[2].length() < 1000) {
            s[2] = T20_FactorialDigits.getSum(s[0], s[1]);
            s[0] = s[1];
            s[1] = s[2];
            i++;
        }

        System.out.println(i);
    }
}
