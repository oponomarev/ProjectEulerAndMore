package edu.mipt.ponomarev.euler;

import org.omg.PortableInterceptor.SYSTEM_EXCEPTION;

import java.util.ArrayList;

/**
 * Created by Oleg on 26.12.2015.
 */
public class T47_DistinctPrimesFactors {
    public static ArrayList<Integer> primeNumbers = T35_CircularPrimes.getPrimeNumbers(1000);
    public static int numberOfDistinctPrimeFactors (int input, int enough){
        if (primeNumbers.get(primeNumbers.size() - 1) < input){
            primeNumbers = T35_CircularPrimes.getPrimeNumbers(input * 2);
        }
        int count = 0;
        for (Integer i : primeNumbers){
            if (input % i == 0){
                count++;
            }
            if (count == enough){
                break;
            }
        }
        return count;
    }
    public static void main (String[] args){
//        System.out.println(numberOfDistinctPrimeFactors(644, 6));
        int limit = 4;
        int count = 0;
        int i;
        for (i = 2; count < limit; i++){
            if (limit == numberOfDistinctPrimeFactors(i, limit)){
                count++;
            } else {
                count = 0;
            }
            if (i == 2147483647){
                break;
            }
            if (i % 10000 == 0){
                System.out.println(i);
            }
        }
        System.out.println(i-limit);
    }
}
