package edu.mipt.ponomarev.euler;

import java.math.BigInteger;
import java.util.ArrayList;

/**
 * Created by Oleg Ponomarev on 30.11.2015.
 */
public class T65_ConvergentsOfE
{
    public static int[] getDivisors (int a) {
        int[] divs;
        ArrayList<Integer> divisors = new ArrayList<Integer>();
        ArrayList<Integer> primes = T35_CircularPrimes.getPrimeNumbers(a);
        if (primes.contains(a)) {
            divisors.add(a);
        } else {
            int i = 0;
            while (a > 1) {
                if (a % primes.get(i) == 0) {
                    divisors.add(primes.get(i));
                    a = a / primes.get(i);
                } else {
                    i++;
                }
            }
        }
        divs = new int[divisors.size()];
        for (int i = 0; i < divisors.size(); i++) {
            divs[i] = divisors.get(i);
        }
        return divs;
    }

    public static int getLCD (int a, int b) {
        int[] aDivisors = getDivisors(a);
        int[] bDivisors = getDivisors(b);
        int LCD = 1;
        int minLength = aDivisors.length > bDivisors.length ? bDivisors.length : aDivisors.length;
        for (int i = 0; i < minLength; i++) {
            LCD = LCD * aDivisors[i];
            if (aDivisors[i] != bDivisors[i]) {
                LCD = LCD * bDivisors[i];
            }
        }
        if (aDivisors.length > bDivisors.length) {
            for (int i = minLength; i < aDivisors.length; i++) {
                LCD = LCD * aDivisors[i];
            }
        } else {
            for (int i = minLength; i < bDivisors.length; i++) {
                LCD = LCD * bDivisors[i];
            }
        }
        return LCD;
    }

    public static void turnFraction (String[] a) {
        if (a.length == 2) {
            String i = a[1];
            a[1] = a[0];
            a[0] = i;
        }
    }
    public static String getProduct (String s1, String s2){
        ArrayList<String> multiples = new ArrayList<String>();
        for (int i = s2.length()-1; i >= 0; i--){
            String s = T20_FactorialDigits.getMult(s1, (int)s1.charAt(i) - 48);
            for (int j = 0; j < s2.length() - 1 - i; j++){
                s += "0";
            }
            multiples.add(s);
        }
        String sum = "";
        for (int i = 0; i < multiples.size(); i++){
            sum = T20_FactorialDigits.getSum(sum, multiples.get(i));
        }
        return sum;
    }

    public static String[] getStandartFraction (int a, String[] b) {
        String[] fraction = new String[b.length];
        if (b.length == 0 || b == null) {
            fraction = new String[1];
            fraction[0] = "" + a;
        }
        if (b.length == 1) {
            fraction[0] = T20_FactorialDigits.getSum(b[0], ""+a);
        }
        if (b.length == 2) {
            String s = T20_FactorialDigits.getMult(b[1], a);
            fraction[0] = T20_FactorialDigits.getSum(s, b[0]);
            fraction[1] = b[1];
        }
        return fraction;
    }

    public static void main (String[] args) {
        /*int[] b = {1};
        int[] result = getStandartFraction(1, b);
        for (int i = 0; i < result.length; i++){
            System.out.print(result[i]+" ");
        }*/

        int number = 100;
        int[] e = new int[number];
        e[0] = 2;
        int k = 1;
        for (int i = 1; i < e.length; i++) {
            if ((i + 1) % 3 == 0) {
                e[i] = 2 * k;
                k++;
            } else {
                e[i] = 1;
            }
        }
        /*for (int i = 0; i < e.length; i++){
            System.out.print(e[i] + " ");
        }
        System.out.println();*/
        String[] mas = {"1", "" + e[--number]};


        while (number > 1){
            mas = getStandartFraction(e[--number], mas);
            turnFraction(mas);
        }
        mas = getStandartFraction(e[0], mas);
        for (int i = 0; i < mas.length; i++){
            System.out.print (mas[i]+ " ");
        }
        System.out.println();
        int sum = 0;
        for (int i = 0; i < mas[0].length(); i++){
            sum += mas[0].charAt(i) - 48;
        }
        System.out.println("Sum is " + sum);
    }
}
