package edu.mipt.ponomarev.euler;

import static edu.mipt.ponomarev.euler.T18_MaxPathSum1.*;

/**
 * Created by Oleg on 30.07.2016.
 */
public class T67MaxPathSum2 {
    private static final String PATH = "D:\\GoogleDisk\\IdeaProjects\\Euler#2\\src\\main\\resources\\T67.txt";
    private static long[][] input = fillInput(PATH);

    public static void main(String[] args) {
        System.out.println(getSumInACleverWay(input));
    }
}
