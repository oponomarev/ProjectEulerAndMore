package edu.mipt.ponomarev.euler;

/**
 * Created by Oleg Ponomarev on 20.08.2015.
 */
public class T17_NumberLetterCounts {
    public static void main(String args[]) {
        int i,j, sum = 0;
        for (i = 1; i < 1000; i++) {
            if (i/100>0){
                j=i%100;
                sum=sum+getNumber(i/100)+10;
            }
            else {
                j=i;
            }
            if (getNumber(j) > 0) {
                sum = sum + getNumber(j);
            }
            else {
                while (getNumber(j)==0){
                    sum=sum+getNumber(j%10);
                    j=j/10 * 10;
                }
                sum=sum+getNumber(j);
            }
        }
        System.out.println(sum);
    }

    public static int getNumber(int x) {
        switch (x) {
            case 1:
            case 2:
            case 6:
            case 10:
                return 3;
            case 3:
            case 7:
            case 8:
            case 40:
            case 50:
            case 60:
                return 5;
            case 4:
            case 5:
            case 9:
                return 4;
            case 11:
            case 12:
            case 20:
            case 30:
            case 80:
            case 90:
                return 6;
            case 13:
            case 14:
            case 18:
            case 19:
                return 8;
            case 15:
            case 16:
            case 70:
                return 7;
            case 17:
                return 9;
            case 0:
                return -3;
        }
        return 0;
    }
}
