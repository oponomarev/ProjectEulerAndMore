package edu.mipt.ponomarev.euler;

import edu.mipt.ponomarev.euler.util.Utilities;

/**
 * @version 1.0
 * @Author - Oleg Ponomarev
 * @since 19.11.2016
 */
public class T56_PowerfulDigitSum {
    private static int count;

    public static void main ( String[] args ) {
        for (int i = 1; i < 100; i++) {
            for (int j = 1; j < 100; j++) {
                String power = Utilities.getBthPowerOfA( Integer.toString( i ), Integer.toString( j ) );
                checkSum( power );
            }
        }
        System.out.println(count);
    }

    private static void checkSum ( String power ) {
        int current = 0;
        for (int k = 0; k < power.length(); k++) {
            current += power.charAt( k ) - 48;
        }
        if (current > count) {
            count = current;
        }
    }
}
