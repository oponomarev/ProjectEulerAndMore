package edu.mipt.ponomarev.euler;

import java.util.ArrayList;

/**
 * Created by Oleg on 22.01.2016.
 */
public class T43_SubStringDivisibility {
    public static ArrayList<String> pandigitals = pandigitalNumbers();
    public static ArrayList<String> pandigitalNumbers(){
        ArrayList<String> list = new ArrayList<String>();
        for (long i = 1000000000; i < 10000000000L; i++){
            if (isPandigital(i)){
                list.add("" + i);
            }
        }
        return list;
    }
    public static boolean isPandigital(long number){
        String s = "" + number;
        char[] chars = s.toCharArray();
        for (char c = '0'; c < 48 + chars.length; c++){
            if (!T41_PandigitalPrime.contains(chars, c)){
                return false;
            }
        }
        return true;
    }
    public static boolean hasProperty (String s){
        if (Long.parseLong(s.substring(1, 4)) % 2 != 0){
            return false;
        }
        if (Long.parseLong(s.substring(2, 5)) % 3 != 0){
            return false;
        }
        if (Long.parseLong(s.substring(3, 6)) % 5 != 0){
            return false;
        }
        if (Long.parseLong(s.substring(4, 7)) % 7 != 0){
            return false;
        }
        if (Long.parseLong(s.substring(5, 8)) % 11 != 0){
            return false;
        }
        if (Long.parseLong(s.substring(6, 9)) % 13 != 0){
            return false;
        }
        if (Long.parseLong(s.substring(7, 10)) % 17 != 0){
            return false;
        }
        return true;
    }
    public static void main(String[] args){
        long sum = 0;
        for (String s : pandigitals){
            if (hasProperty(s)){
                sum += Long.parseLong(s);
            }
        }
        System.out.println(sum);
    }
}
